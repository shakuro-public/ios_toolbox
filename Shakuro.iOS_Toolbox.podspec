#
#
#

Pod::Spec.new do |s|
    s.name             = 'Shakuro.iOS_Toolbox'
    s.version          = '0.25.0'
    s.summary          = 'A bunch of components for iOS'
    s.homepage         = 'https://github.com/shakurocom/iOS_Toolbox'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.authors          = {'Sanakabarabaka' => 'slaschuk@shakuro.com',
                            'wwwpix' => 'spopov@shakuro.com'}
    s.source           = { :git => 'https://github.com/shakurocom/iOS_Toolbox.git', :tag => s.version }
    s.swift_version    = '5.0'

    s.ios.deployment_target = '10.0'

    s.dependency 'Shakuro.CommonTypes', '1.1.2'

    # --- subspecs ---

    s.subspec 'ContainerViewController' do |sp|

        sp.source_files = 'ContainerViewController/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'Device' do |sp|

        sp.source_files = 'Device/Source/**/*'
        sp.frameworks = 'UIKit', 'CoreMotion'

    end

    s.subspec 'EMail' do |sp|

        sp.source_files = 'EMail/Source/**/*'
        sp.frameworks = 'Foundation'

    end

    s.subspec 'EventHandler' do |sp|

        sp.source_files = 'EventHandler/Source/**/*'
        sp.frameworks = 'Foundation'

    end

    s.subspec 'Exceptions' do |sp|

        sp.source_files = 'Exceptions/Source/**/*'
        sp.frameworks = 'Foundation'

    end

    s.subspec 'ImageProcessing' do |sp|

        sp.source_files = 'ImageProcessing/Source/**/*'
        sp.frameworks = 'CoreGraphics', 'Accelerate', 'AVFoundation', 'UIKit'

    end

    s.subspec 'Keyboard' do |sp|

        sp.source_files = 'Keyboard/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'Keychain' do |sp|

        sp.source_files = 'Keychain/Source/**/*'
        sp.frameworks = 'Foundation'

    end

    s.subspec 'Labels' do |sp|

        sp.source_files = 'Labels/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'PlaceholderTextView' do |sp|

        sp.source_files = 'PlaceholderTextView/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'PoliteCoreData' do |sp|

         sp.source_files = 'PoliteCoreData/Source/**/*'
         sp.frameworks = 'Foundation', 'CoreData'

    end

    s.subspec 'PullToRefresh' do |sp|

        sp.source_files = 'PullToRefresh/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'Router' do |sp|

         sp.dependency 'Shakuro.iOS_Toolbox/ContainerViewController'
         sp.source_files = 'Router/Source/**/*'
         sp.frameworks = 'UIKit'

    end

    s.subspec 'ScrollableTabs' do |sp|

        sp.source_files = 'ScrollableTabs/Source/**/*'
        sp.frameworks = 'UIKit'

    end

    s.subspec 'Settings' do |sp|

        sp.source_files = 'Settings/Source/**/*'
        sp.frameworks = 'Foundation'

    end

    s.subspec 'Utils' do |sp|

        sp.source_files = 'Utils/Source/**/*'
        sp.frameworks = 'Foundation'

    end

    s.subspec 'Vibrations' do |sp|

        sp.dependency 'Shakuro.iOS_Toolbox/Device'
        sp.source_files = 'Vibrations/Source/**/*'
        sp.frameworks = 'AudioToolbox', 'Foundation', 'UIKit'

    end

    s.subspec 'VideoCamera' do |sp|

        sp.dependency 'Shakuro.iOS_Toolbox/Device'
        sp.dependency 'Shakuro.iOS_Toolbox/ImageProcessing'
        sp.source_files = 'VideoCamera/Source/**/*'
        sp.frameworks = 'Accelerate', 'AVFoundation', 'UIKit'

    end

end
